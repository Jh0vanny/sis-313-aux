
# Practica_N°10 Template para react
|            |            |            |            |  
|:----------:|:----------:|:----------:|:----------:|
| Estudiante:| Jhovanny Dulfredo Pimentel MAmani  | Materia:|Diseño y Progamacion Gráfiaca|
| Auxiliar:|Zergio Moises Apaza Caballero | Dosente:|ing. José David Mamani Figueroa|





#### 1.Para la práctica, subir un archivo.md como se indica en la parte de NOTA(S) (es decir, debe llevar el nombre "HW10_templateIdeas.md").
Plan
Presentar lo siguiente:
- Link del template.
- Link del proyecto en Figma utilizando el template.
- Link del repositorio creado:

## - Link del template.

```http
https://www.figma.com/community/file/1354651678737219705
```
## - Link del proyecto en Figma utilizando el template.

```http
   https://www.figma.com/file/iMjctQ2EBe7ubYXFivVkuO/Cowork---Coworking-Space-Company-(Community)?type=design&node-id=1%3A166&mode=design&t=h92piW5ccodmKXFN-1
```
## - Link del repositorio creado:
```http
 https://gitlab.com/Jh0vanny/react-with-jhovanny_dulfredo_pimentel_mamani.git
```