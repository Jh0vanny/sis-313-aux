
# Practica_N°7_Terminales_y_Comandos
|            |            |            |            |  
|:----------:|:----------:|:----------:|:----------:|
| Estudiante:| Jhovanny Dulfredo Pimentel MAmani  | Materia:|Diseño y Progamacion Gráfiaca|
| Auxiliar:|Zergio Moises Apaza Caballero | Dosente:|ing. José David Mamani Figueroa|





## 1. Realice un video explicando lo siguiente:
   - a. La creación de una carpeta en el disco local C:, mediante comandos desde la terminal video
   - b. La forma de cambiar de directorios utilizando comandos 

https://drive.google.com/file/d/1EDWLK_6BCtVi5yHwAoQPp6Sn6tc63RSu/view?usp=drive_link


## 2. Adjunte capturas de pantalla del mismo proceso, tres veces más.
![App Screenshot](PRACTICA7/imagenes/1.jpg)
![App Screenshot](PRACTICA7/imagenes/2.jpg)
## 3. Que comandos se utilizó en las anteriores preguntas.

#### -Creacion de carpetas

```http
 mkdir C:\Tarea_N°7
```
#### - Cambiar directorios:

  - forma general

```http
  cd C:\Users\User
```
- directorio que cambiamos para el ejemplo:

```http
  cd C:
```
## 4. Explique 5 comandos para ser utilizados en la terminal y su forma de usarse en Windows (Terminal o Powershell) y en Linux (bash).

#### 1. Comando: cd (Change Directory):

  - Windows: Se utiliza para cambiar el directorio actual.

```http
cd [ruta_del_directorio]
```
  - Linux (bash): También se utiliza para cambiar el directorio actual.

```http
 cd [ruta_del_directorio]
```

#### 2. Comando: mkdir (Make Directory):

  - Windows: Se utiliza para crear un nuevo directorio.

```http
  Windows: Se utiliza para crear un nuevo directorio
```
  - Linux (bash): También se utiliza para crear un nuevo directorio.

```http
  mkdir [nombre_del_directorio]
```

#### 3. Comando: rm (Remove):

  - Windows: En PowerShell, se utiliza para eliminar archivos o directorios.

```http
  Remove-Item [ruta_del_archivo_o_directorio]
```
  - Linux (bash): Se utiliza para eliminar archivos o directorios.

```http
  rm [ruta_del_archivo_o_directorio]
```

#### 4. Comando: ls (List):

  - Windows: En PowerShell, se utiliza para enumerar el contenido de un directorio.

```http
  Get-ChildItem [ruta_del_directorio]
```
  - Linux (bash): Se utiliza para listar el contenido de un directorio.

```http
  ls [ruta_del_directorio]
```
#### 5. Comando: cp (Copy):

  - Windows: En PowerShell, se utiliza para copiar archivos o directorios.

```http
  Copy-Item [ruta_del_origen] [ruta_del_destino]
```
  - Linux (bash): Se utiliza para copiar archivos o directorios.

```http
  cp [ruta_del_origen] [ruta_del_destino]
```
