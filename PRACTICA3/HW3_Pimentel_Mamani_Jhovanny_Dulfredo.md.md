## Pracctica N°3

**Nombre:**
Jhovanny Dulfredo Pimentel Mamani

**Nivel Medio** 
![ejercicio 21](PRACTICA3/imagenes/1m.jpg)
![ejercicio 22](PRACTICA3/imagenes/2m.jpg)
![ejercicio 23](PRACTICA3/imagenes/3m.jpg)
![ejercicio 24](PRACTICA3/imagenes/4m.jpg)

**Nivel Dificil**
![ejercicio 21](PRACTICA3/imagenes/1d.jpg)
![ejercicio 22](PRACTICA3/imagenes/2d.jpg)
![ejercicio 23](PRACTICA3/imagenes/3d.jpg)
![ejercicio 24](PRACTICA3/imagenes/4d.jpg)