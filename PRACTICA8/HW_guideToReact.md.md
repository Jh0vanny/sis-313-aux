
# Practica_N°8_Creación_de un _proyecto_con_React
|            |            |            |            |  
|:----------:|:----------:|:----------:|:----------:|
| Estudiante:| Jhovanny Dulfredo Pimentel MAmani  | Materia:|Diseño y Progamacion Gráfiaca|
| Auxiliar:|Zergio Moises Apaza Caballero | Dosente:|ing. José David Mamani Figueroa|





## 1. Adjunte capturas de pantalla mostrando la versión de node y de npm mediante comandos por la terminal

![App Screenshot](PRACTICA8/imagenes/1.jpg)

## 2. Realice un video explicando lo siguiente:
- La creación de una carpeta en el disco local C:, mediante comandos desde la terminal
- La creación de un proyecto utilizando React con Next.js
**NOTA:** El video solo deberá explicar la creación de UN SOLO proyecto

https://drive.google.com/file/d/1vVtl4SAL4fl5t9tcJI3U8nX-TTFvEXMc/view?usp=drive_link

## 3. ¿Qué comandos se utilizó en las anteriores preguntas?

#### -camviar el directorio al disco loca C:

```http
 cd C:\
```
#### - Creacion del Proyecto copiando de la pagina de React

```http
  npx create-next-app@latest
```
- son todos los comamndos que se utilizo en la creacion del proyecto 
## 4. ¿Qué errores tuvo al crear el proyecto? Tome en cuenta los errores que tuvo al crear el proyecto PARA ESTA PRÁCTICA y para el LABORATORIO DEL DÍA MARTES, 16 DE ABRIL DE 2024.
 - IMPORTANTE: De preferencia, adjunte las capturas de dichos errores.

![App Screenshot](PRACTICA8/imagenes/2.jpg)

## 5. Existen ocasiones, en las en el repositorio (gitlab), una carpeta se torna de color naranja y no se puede acceder al mismo, tal y como se muestra en la imagen. Este mismo proyecto no permite ser clonado y en caso de éxito en la clonación, la carpeta aparece vacía.
 - **Averigüe la causa del problema y la solución del mismo.**

 - La carpeta naranja en GitLab indica que es un submódulo git. Los submódulos son repositorios git incrustados dentro de otro repositorio git más grande. Cuando la carpeta aparece naranja y no se puede acceder, puede significar que el submódulo no se ha inicializado correctamente o que no se ha clonado junto con el repositorio principal.
 - **Solucion:**

 #### 1. Inicializar los submódulos: Si no se han inicializado los submódulos correctamente, puedes hacerlo usando el comando:
```http
 git submodule update --init
```
#### 2. Clonar el repositorio con submódulos: Si estás clonando un repositorio que contiene submódulos, asegúrate de usar la opción --recursive para asegurarte de que los submódulos se clonen correctamente junto con el repositorio principal. Por ejemplo:

```http
  git clone --recursive <https://gitlab.com/Zergio23/aux-sis313g1-i24.git>
```
#### 4. Actualizar los submódulos: Si ya has clonado el repositorio principal pero los submódulos no se han clonado correctamente, puedes actualizarlos manualmente con el comando:

```http
  git submodule update --recursive
```
### Estos pasos deverian solucionar este problema otra causa seria que la carpeta no exista o su ruta es incorrecta tambien no tener los permisos nesesarios