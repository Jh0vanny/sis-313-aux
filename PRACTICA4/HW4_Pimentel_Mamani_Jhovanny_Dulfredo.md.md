# Práctica N°4

**Nombre:** 
Jhovanny Dulfredo Pimentel Mamani

**Grupo:** 1

## Pregunta N°1

***Complete los niveles (Hard) Difícil y adjunte capturas de los siguientes 13
niveles vencidos:
10, 18, 20, 29, 30, 40, 45, 50, 60, 66, 70, 78 y 80.**

- **Ejercicio 10**
![ejercicio 10](PRACTICA4/imagenes/1.jpg)

- **Ejercicio 18**
![ejercicio 18](PRACTICA4/imagenes/2.jpg)

- **Ejercicio 20**
![ejercicio 20](PRACTICA4/imagenes/3.jpg)

- **Ejercicio 29**
![ejercicio 29](PRACTICA4/imagenes/4.jpg)

- **Ejercicio 30**
![ejercicio 30](PRACTICA4/imagenes/5.jpg)

- **Ejercicio 40**
![ejercicio 40](PRACTICA4/imagenes/6.jpg)

- **Ejercicio 45**
![ejercicio 45](PRACTICA4/imagenes/7.jpg)

- **Ejercicio 50**
![ejercicio 50](PRACTICA4/imagenes/8.jpg)

- **Ejercicio 60**
![ejercicio 60](PRACTICA4/imagenes/9.jpg)

- **Ejercicio 66**
![ejercicio 66](PRACTICA4/imagenes/10.jpg)

- **Ejercicio 70**
![ejercicio 70](PRACTICA4/imagenes/11.jpg)

- **Ejercicio 78**
![ejercicio 78](PRACTICA4/imagenes/12.jpg)

- **Ejercicio 80**
![ejercicio 80](PRACTICA4/imagenes/13.jpg)